Raspberry Pi Cross Compilation Tools
====================================

Building the toolchain automatically
------------------------------------

For typical usage it should be sufficient to build the toolchain using the
default settings:

```bash
git clone https://bitbucket.org/trotterdylan/rpi-x-tools.git
cd rpi-x-tools
./install.sh
source rpi-x-tools.rc
```

Manually building the toolchain
-------------------------------

For those interested in the process underlying the default install or wanting
to override some aspects of the config, here's an alternative approach:

### Get RPi-X-Tools

```bash
git clone https://bitbucket.org/trotterdylan/rpi-x-tools.git
cd rpi-x-tools
# This is used in steps below
export RPI_X_TOOLS_DIR=`pwd`
```

### Install required packages

```bash
sudo apt-get install gperf bison flex texinfo gawk libtool automake subversion ncurses-dev g++
```

### Build crosstool-ng

```bash
cd /tmp
wget http://crosstool-ng.org/download/crosstool-ng/crosstool-ng-1.19.0.tar.bz2 -O - | tar -jxvf -
cd crosstool-ng-1.19.0
./configure --prefix=$RPI_X_TOOLS_DIR/crosstool-ng-1.19.0
make
make install
export PATH="${PATH}:$RPI_X_TOOLS_DIR/crosstool-ng-1.19.0/bin"
```

### Build the toolchain

```bash
cd $RPI_X_TOOLS_DIR/crosstool-ng-config
ct-ng oldconfig
ct-ng build
export PATH=${PATH}:$RPI_X_TOOLS_DIR/arm-rpi-linux-gnueabihf/bin
```

This will use the default toolchain settings. Use `ct-ng menuconfig` in place
`ct-ng oldconfig` above to choose the toolchain configuration options. See below
for some discussion on the default options.

crosstool-NG configuration
--------------------------

### ct-ng menuconfig

The crosstool-ng-config directory referenced above contains a configuration
file for crosstool-ng. It was generated with `ct-ng menuconfig` and the
following options:

* Paths and misc options
    - Set "Try features marked as EXPERIMENTAL"
    - Set "Number of parallel jobs" to something reasonable

* Target options:
    - Set "Target architecture" to "ARM"
    - Set "Architecture level" to "armv6"
    - Set "Use specific FPU" to "vfp"
    - Check "append 'hf' to the tuple"

* General toolchain:
    - Set "Tuple's vendor string" to "rpi"

* Operating system:
    - Set "Target OS" to "linux"
    - Set "Linux kernel version" to "3.10.2"

* C compiler:
    - Check "Show Linaro versions"
    - "gcc version" to "linaro-4.8-2013.06-1"
    - Check "C++"
    - Set "gcc extra config" to "--with-float=hard"

### Configuration notes

* Some sources online suggest setting "Architecture level" to "armv6zk" and
  "Emit assembly for CPU" to "arm1176jzf-s". These seem to cause some problems
  at runtime. Using plain "armv6" and no "Emit assembly for CPU" setting seem
  more reliable.

* It seems preferable to use the same GCC version as is on Raspian (currently
  4.6) since it will mean code written to compile in one environment will work
  on the other but I ran into a couple problems with 4.6 that seem to be
  resolved in 4.8:

    1. vfp and hardfloat [are not compatible][1], and
    2. [There is a bug in PPL][2] that makes GMP 5.1 incompatible with the
       default PPL version (0.11.)

[1]: http://communities.mentor.com/community/cs/archives/arm-gnu/msg00356.html
[2]: http://www.cs.unipr.it/pipermail/ppl-devel/2012-December/018576.html
