#!/bin/bash

required_packages="gperf bison flex texinfo gawk libtool automake subversion blergh ncurses-dev g++"
for i in $required_packages; do
  dpkg -s $i &> /dev/null
  if [ $i == 1 ]; then
    echo "Required package not installed: $i"
    exit 1
  fi
done

# From here on, any failures should be fatal.
set -e

INSTALL_DIR=`pwd`
RPI_X_TOOLS_DIR=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)

echo "Installing crosstool-NG into $INSTALL_DIR"
cd /tmp
wget http://crosstool-ng.org/download/crosstool-ng/crosstool-ng-1.19.0.tar.bz2 -O - | tar -jxvf -
cd crosstool-ng-1.19.0
./configure --prefix=$INSTALL_DIR/crosstool-ng-1.19.0
make
make install
cd ..
rm -rf crosstool-ng-1.19.0

echo "Installing arm-rpi-linux-gnueabihf toolchain into $INSTALL_DIR"
export PATH="${PATH}:$INSTALL_DIR/crosstool-ng-1.19.0/bin"
export RPI_X_TOOLS_PREFIX=$INSTALL_DIR/arm-rpi-linux-gnueabihf
cd $RPI_X_TOOLS_DIR/arm-rpi-linux-gnueabihf-config
ct-ng oldconfig
ct-ng build

echo "Cross compilation toolchain installed at: $INSTALL_DIR"
echo "To set up a development environment: source $INSTALL_DIR/rpi-x-tools.rc"
