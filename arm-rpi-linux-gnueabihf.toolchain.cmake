set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 3.10)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_C_COMPILER arm-rpi-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER arm-rpi-linux-gnueabihf-g++)

# The directory containing libraries compiled with the ARM toolchain.
set(ARM_LINUX_SYSROOT "$ENV{RPI_X_TOOLS_PREFIX}" CACHE PATH "ARM cross compilation system root")
set(CMAKE_FIND_ROOT_PATH "${ARM_LINUX_SYSROOT}")

# Look for libraries and headers *only* in the ARM cross compilation system root.
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)

# Look for binaries on the host system.
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# Don't ever set an rpath.
set(CMAKE_SKIP_RPATH TRUE CACHE BOOL "If set, runtime paths are not added when using shared libraries." )
